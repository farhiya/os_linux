# OS and Linux Intro

## Operating System (OS)

OS are a level of abstraction on the bare metal.

Some have GUI such as Andoid, Microsoft, MacOs and lots of linux distribution. Others do not.

We will be using Linux a lot.

## Linux

there are many linux distributions. Here are some:
    
- ubutu(debian) - user friendly, widely used
- federo
- redhat distros (RHEL)
- arch 
- kali (penetration testing/ethical hacking)

### Bash and shells

Linux comes with bash terminal and the might come with differnt shells.

Different shells behave slightly differnt.

- bash
- oh-my-zsh
- gitbash
- others

small differences in some commands.

### Package managers

perfomed through the package manager

- RHEL = yum , dnf or rmp
- Debian = apt-get, aptitude or dpkg

These help install software - Such as python nginx and apache.


### Aspects

Everything is a file

- Everything can interact with everything - this is great for automation

### 3 file descriptors

To ineract with programs and files we can use redirect and play around with 0, 1 and 2 that are stdin, stdout and stderr

**Standard input  (stdin)**

**Standard  output (stdout)**

**Standard  error (stderr)**


### Two Important Hard paths `/` and `~`

`/` - means root directory 

`~`- means `/users/your_login_user/`




### Environment variables
Environment is wher code runs.
In bash we might want to set some variables.

Avariable is like a box you name and you assign something(like a value ) to it, to use later.

In bash we define a variable simple by:

``` bash

# setting a variable

 My_Var="This is a Variable"

# call the variable

echo $My_Var
> "This is a Variable"

# you can reassign  the variable

My_Var="Hello"
echo $My_Var
> "Hello"

# call other variables the same way

echo $USER
>Farhiya


When I close my terminal, variables not in the path will be lost.

```
#### Child process

A process that's initaited under another process like running another bash script.

It goes via the $PATH but in a newterminal essentially, when our set variables do not exist in the new child terminal.

so in our bash_file :
``` bash

echo "Hi from file"

echo $LANDLORD

echo "Landlord above"
```

when we run the bash file in our terminal the `echo $LANDLORD` value will not display but the other 2 lines of code will display in our terminal.

### Path


Terminal and Bash process follow the PATH.

There is a PATH for login users that have a profile and the others without.

Files to consider:

```bash 
# these always exist at location ~
> .zprofile
> .zshrc

# for bash shell without oh-my-zsh:
> .bashrc
> .bash_profile
> .bashprofile 


```


# check variable on terminal
```bash
$ ENV

# chaninge permissions

chmod +x bash_file.sh

```

#### Setting and defining Environment variables

You need to add them to a file on the path and export them.

#### common commands
```bash
# check variable on terminal 
$ ENV


# Changeing permisiion 

$ chmod +rwx <files>

```
#### Permissions

### Users and Sudoes 


## Wild cards 

### Matchers

You can use this to help you match files or content in files. 

```
# any number of character to a side 
ls exam*
> example     example.jpg example.txt example2

ls *exam
> this_is_an_exam

# ? for specific number of charecters
ls example????
> example.jpg example.txt

## List of specific characters
### each one of these represents 1 character [a-z]
ls example.[aeiout][a-z][a-z]
> example.txt
```

### Redirects 

Every command has 3 defuats.

- stdin - Represent 0
- stdout - Represent 1
- stderr - Represent 2

example of stdout:
```bash
ls
> README.md       example         example.txt     notes (...)
## the list that prints is the stdout!
```

example giving `ls` a stdin - the example????

```bash 
# the example???? is the stdin
ls example????
> example.jpg example.txt
# the output is stdout
```

If there is an error, you get a stderr:
```bash
ls example???
> zsh: no matches found: example???
# the above is a stderr
```

The cool thing is you can redirect this! 

##### > and >>

This `>` will redirect and truncate.

This `>>` will redirect and append. 

Use it with number to decided what to redirect.

#### messing with STDIN 

`<`

### Pipes 

Piping redirect and makes it stdin of another program 


### Demo using > and >>


Using > will replace what's in the file
Using >> will appened to a file 


#### Replacing to file (>)

##### Demo using > and >> to redirect the stdout to file and strerr to another file.

```bash


`ls /etc/[a-zA-Z]*[0-9]`
> # outputs everything in etc

# This command will move an output to file good.txt and and error message to bad.txt. If the txt files don't exist it will create them.

`ls -l /etc >good.txt 2>bad.txt`

#at this point good.txt has the ect contents and bad.txt is empty.

# bad.'txt and good.txt files now exist
`ls`
 >`bad.txt  good.txt`

# show me what's in bad.txt file
`cat bad.txt`
> # no output , file is empty

`cat good.txt`
> # outputs everything in etc

# Now if we input a command that has an error it will go into the bas.txt file.

# Here we ask to see what's in 'baddirectory'. As it does not exist, there is an error. That error messeage is stored in bad.txt.

`ls -l baddirectory >good.txt 2>bad.txt`

#at this point good.txt is empty because we replace the contents with a null output and bad.txt has contains the error message

`cat bad.txt`
> `ls: cannot access 'baddirectory': No such file or directory`


```
#### Apending to the file

If I want to put contents of bad.txt into good.txt:

contents of file_1: this is 1

contents of file_2: this is 2

```bash

# contents of file 1 is appened to file 2
`cat file_1.txt >> file_2.txt`

`cat file_1.txt`
> this is file 1

`cat file_2.txt`
>this is file 2
>this is file 1

```

### Demo stdout and stderr into the same file

```bash

`ls -l grep | file >out2>&1` -> #stdout
`ls -l |gre file>out 2>&1` -> #stderr

```

### Demo runnning processes in the background


```bash

`sleep 20 &`
> [1] 6944
# Terminal is nnow running in the background

`ps aux | grep sleep`
> # displays all sleep

`killall sleep`
# all sleep terminals are closed
```


